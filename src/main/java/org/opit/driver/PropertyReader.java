package org.opit.driver;

import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    private static PropertyReader instance = null;
    private Properties properties;

    private PropertyReader() {
        try {
            String appName = System.getProperty("appName", "app");
            properties = new Properties();
            properties.load(PropertyReader.class.getClassLoader().getResourceAsStream("app.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized static PropertyReader getInstance() {
        if (instance == null) {
            instance = new PropertyReader();
        }
        return instance;
    }

    public String getProperty(String propertyName){
        return properties.getProperty(propertyName);
    }


}
