package org.opit.driver;

public enum Browsers {
    CHROME,
    FIREFOX,
    EDGE
}
