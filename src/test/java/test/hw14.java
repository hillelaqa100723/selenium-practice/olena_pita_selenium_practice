package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;

import java.util.Set;

public class hw14 {
        WebDriver driver;

        @BeforeClass
        public void beforeClass() {
            driver = new ChromeDriver();
        }

        @AfterClass(alwaysRun = true)
        public void afterClass() {
            if (driver != null) {
                driver.quit();
            }
        }
        @BeforeMethod
        public void beforeMethod() {
            driver.get("http://the-internet.herokuapp.com/windows");
        }

        @Test
        public void windowsTest(){

            String mainWindowHandle = driver.getWindowHandle();

            driver.findElement(By.linkText("Click Here")).click();

            Set<String> windowHandles = driver.getWindowHandles();
            for (String handle: windowHandles){
                if (!handle.equals(mainWindowHandle)){
                    driver.switchTo().window(handle);
                    break;
                }
            }

            String text = driver.findElement(By.xpath("//h3")).getText();
            String currentUrl = driver.getCurrentUrl();

            Assert.assertEquals(text, "New Window");
            Assert.assertTrue(currentUrl.endsWith("/new"));

            driver.close();

            driver.switchTo().window(mainWindowHandle);

            text = driver.findElement(By.xpath("//h3")).getText();

            Assert.assertEquals(text, "Opening a new window");
            Assert.assertTrue(driver.findElement(By.linkText("Click Here")).isDisplayed());

        }
    }

