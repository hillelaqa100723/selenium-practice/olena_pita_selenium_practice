package test;

import org.openqa.selenium.*;
import org.opit.driver.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class hw11_12 extends BaseClass{

    @BeforeMethod
    public void beforeMethod() {
        driver.get(PropertyReader.getInstance().getProperty("baseUrl") + "/javascript_alerts");
    }


    @Test(description = "Click on OK button for JS Confirm")
    public void jsAlertConfirm() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";


        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);

    }

    @Test(description = "Click on Cancel button for JS Confirm")
    public void jsAlertCancel() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";


        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);


    }

    @Test (description = "Add test to JS prompt")
    public void jsPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "text";
        clickJSButton(Buttons.JS_PROMPT);


        String textFromAlert = workWithAlert(true, testToEnter);


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }


    @Test(description = "Check empty for JS Prompt")
    public void jsPromptEmpty() {
        String expectedTextAtAlert = "I am a JS prompt";


        clickJSButton(Buttons.JS_PROMPT);


        String textFromAlert = workWithAlert(true, "");


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:", resultText);
    }


    @Test(description = "Add text to JS Prompt and click Cancel")
    public void JsPromptPlusCancel() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "text";


        clickJSButton(Buttons.JS_PROMPT);


        String textFromAlert = workWithAlert(false, testToEnter);


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);

    }

    @Test(description = "Check empty field plus Cancel for JS Prompt")
    public void JsPromptEmptyPlusCancel() {
        String expectedTextAtAlert = "I am a JS prompt";


        clickJSButton(Buttons.JS_PROMPT);


        String textFromAlert = workWithAlert(false, "");


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);

    }

    @Test
    public void jsAlertTestCollingJS() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";


        clickJSButtonCallingJS(Buttons.JS_ALERT);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }


    @Test
    public void jsAlertTestWithJS() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";


        clickLSButtonWithJs(Buttons.JS_ALERT);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }


    @Test
    public void jsConfirmTestCollingJS() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";


        clickJSButtonCallingJS(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }


    @Test
    public void jsConfirmTestWithJS() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";


        clickLSButtonWithJs(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }


    @Test
    public void jsPromptTestCollingJS() {
        String expectedTextAtAlert = "I am a JS prompt";
        String expectedResultText = "You entered:";


        clickJSButtonCallingJS(Buttons.JS_PROMPT);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }


    @Test
    public void jsPromptTestWithJS() {
        String expectedTextAtAlert = "I am a JS prompt";
        String expectedResultText = "You entered: ";


        clickLSButtonWithJs(Buttons.JS_PROMPT);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }


    private void clickJSButton(Buttons buttons) {
        findButton(buttons).click();
    }


    private void clickJSButtonCallingJS(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("return " + buttons.getCode());
    }


    private void clickLSButtonWithJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement button = findButton(buttons);
        executor.executeScript("return arguments[0].click();", button);
    }

    private WebElement findButton(Buttons button) {
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }


    private String workWithAlert(boolean accept, String... textToInput) {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();


        if (textToInput.length > 0) {
            alert.sendKeys(textToInput[0]);
        }


        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }


    private String getResultText() {
        return driver.findElement(By.id("result")).getText();
    }


    private String getResultTextWithJs() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("result"));
        return executor.executeScript("return arguments[0].textContent;", element).toString();
    }


    enum Buttons {
        JS_ALERT("Click for JS Alert", "jsAlert()"),
        JS_CONFIRM("Click for JS Confirm", "jsConfirm()"),
        JS_PROMPT("Click for JS Prompt", "jsPrompt()");


        private String textValue;
        private String code;


        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;


        }


        public String getTextValue() {
            return textValue;
        }

        public String getCode() {
            return code;
        }
    }
}
