package test.Homework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class hw13 {

    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new ChromeDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/nested_frames");
    }

    @DataProvider
    public Object[] dataProvider() {
        return new Object[][]{
                {"frame-left", "LEFT"},
                {"frame-middle", "MIDDLE"},
                {"frame-right", "RIGHT"},
                {"frame-bottom", "BOTTOM"},
        };
    }

    @Test(dataProvider = "dataProvider")
    public void verifyTextInFrames(String frameName, String frameTextExpected) {
        if (!frameName.equals("frame-bottom")) {
            driver.switchTo().frame("frame-top");
        }
            driver.switchTo().frame(frameName);
        
            String frameText = driver.findElement(By.tagName("body")).getText().trim();
            Assert.assertEquals(frameText, frameTextExpected);
        }
    }



