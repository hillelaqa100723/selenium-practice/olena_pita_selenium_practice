package test;

import org.openqa.selenium.WebDriver;
import org.opit.driver.WebDriverFactory;
import org.opit.utils.MyFileUtils;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseClass {
   protected WebDriver driver;


    @BeforeSuite
    public void beforeClass() {
        MyFileUtils.getDownloadsFolder();
        driver = WebDriverFactory.initDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }
}


