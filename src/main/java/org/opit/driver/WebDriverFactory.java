package org.opit.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.opit.utils.MyFileUtils;

import java.util.HashMap;

public class WebDriverFactory {

    public static WebDriver initDriver(Browsers browser) {
        switch (browser) {
            case CHROME -> {
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("download.default_directory", MyFileUtils.getDownloadsFolder().getAbsolutePath());

                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", chromePrefs);

                return new ChromeDriver(options);
            }
            case FIREFOX -> {
                return new FirefoxDriver();
            }
            case EDGE -> {
                return new EdgeDriver();
            }
        }
        return null;
    }

    public static WebDriver initDriver() {

        String browserToRun = System.getProperty("browserToRun", "chrome");

        {
            try {
                Browsers browser = Browsers.valueOf(browserToRun.toUpperCase());
                return initDriver(browser);
            } catch (IllegalArgumentException e) {
                System.err.println(browserToRun + "is not supported");
                System.exit(-1);
            }
            return null;
        }
    }
}

